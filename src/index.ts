//----------------------------------------------------------------------------------------
//  Image processing with WebGLUT
//----------------------------------------------------------------------------------------

/// <reference types="../vendor/webgl-debug" />

import { util } from 'webglut'
import { throwOnGLError, logAndValidate, makeRenderingContext } from './util'

import { loadImage } from './util'
import * as kernel from './kernel'

if (util.isDefined(WebGLDebugUtils)) {
    // Ensure no WebGL library calls are made until the WebGL Inspector is ready
    window.addEventListener('gliready', main)
} else {
    document.addEventListener('DOMContentLoaded', main)
}

function main() {
    let _gl = makeRenderingContext()
    if (_gl === null) {
        throw 'Failed to get WebGLRenderingContext'
    }
    if (util.isDefined(WebGLDebugUtils)) {
        _gl = WebGLDebugUtils.makeDebugContext(_gl, throwOnGLError, logAndValidate)
        _gl = _gl as WebGLRenderingContext
    }
    const gl = _gl

    loadImage('res/bridge.jpg', (image) => {
        let width = image.width
        let height = image.height
        let ratio = width / height

        width = window.innerWidth - 20
        height = width / ratio

        setupImageDestination('identity', width, height)
        new kernel.Pipeline(gl, image)
            .intoImage('identity')

        setupImageDestination('edgeDetection1', width, height)
        new kernel.Pipeline(gl, image)
            .pipe(kernel.edgeDetectKernel1)
            .intoImage('edgeDetection1')

        setupImageDestination('edgeDetection2', width, height)
        new kernel.Pipeline(gl, image)
            .pipe(kernel.edgeDetectKernel2)
            .intoImage('edgeDetection2')

        setupImageDestination('edgeDetection3', width, height)
        new kernel.Pipeline(gl, image)
            .pipe(kernel.gaussianBlurKernel(5))
            .pipe(kernel.edgeDetectKernel3)
            .intoImage('edgeDetection3')
    })
}

function setupImageDestination(imageId: string, width: number, height: number) {
    let image = document.createElement('img')
    image.id = imageId
    document.body.appendChild(image)

    image.width = width
    image.height = height
}