import { gfx, tex, util } from 'webglut'

import { ProgramInfo } from 'webglut/dist/gfx';

export const identityKernel = [
    0, 0, 0,
    0, 1, 0,
    0, 0, 0,
]

export const edgeDetectKernel1 = [
     1,  0, -1,
     0,  0,  0,
    -1,  0,  1,
]

export const edgeDetectKernel2 = [
     0,  1,  0,
     1, -4,  1,
     0,  1,  0,
]

export const edgeDetectKernel3 = [
    -1, -1, -1,
    -1,  8, -1,
    -1, -1, -1,
]

export function boxBlurKernel(size: number): number[] {
    let res = []
    for (let i = 0; i < size * size; i++) {
        res.push(1)
    }
    return res
}

export function gaussianBlurKernel(size: number): number[] {
    let normal = (x: number): number => {
        let coeff = 1 / Math.sqrt(2 * Math.PI)
        let power = -Math.pow(x, 2) / 2
        return coeff * Math.exp(power)
    }

    let start = -3
    let delta = 6 / (size - 1)

    let res = []
    for (let j = 0; j < size; j++) {
        for (let i = 0; i < size; i++) {
            let x = start + (i * delta)
            let y = start + (j * delta)
            let fx = Math.sqrt(normal(x))
            let fy = Math.sqrt(normal(y))
            res.push(fx * fy)
        }
    }
    return res
}

export const sharpenKernel = [
     0, -1,  0,
    -1,  5, -1,
     0, -1,  0,
]

function makeKernelState(gl: WebGLRenderingContext): gfx.State {
    const attribs = [
        -1.0, -1.0,     0.0, 0.0,
         1.0, -1.0,     1.0, 0.0,
        -1.0,  1.0,     0.0, 1.0,
        -1.0,  1.0,     0.0, 1.0,
         1.0, -1.0,     1.0, 0.0,
         1.0,  1.0,     1.0, 1.0,

    ]

    return {
        attribCount: 6,
        attribOffset: 0,
        attributes: {
            positions: {
                data: attribs,
                layout: {
                    size: 2,
                    type: gl.FLOAT,
                    stride: 4 * 4,
                    offset: 0,
                },
            },
            texCoords: {
                data: attribs,
                layout: {
                    size: 2,
                    type: gl.FLOAT,
                    stride: 4 * 4,
                    offset: 2 * 4,
                },
            },
        },
    }
}

export function makeKernelPipeline(
    gl: WebGLRenderingContext, kernelState: gfx.State, kernel: number[]): gfx.Pipeline
{
    let pipeline = {
        programInfo: makeKernelShader(gl, kernel),
        state: kernelState,
        dataMap: {
            positions: 'position',
            texCoords: 'texCoord',
        },
    }

    let kernelLoc = pipeline.programInfo.uniformLocations.kernel
    if (! util.isDefined(kernelLoc)) {
        throw 'Uniform location is undefined'
    }

    let kernelWeightLoc = pipeline.programInfo.uniformLocations.kernelWeight
    if (! util.isDefined(kernelWeightLoc)) {
        throw 'Uniform location is undefined'
    }

    gfx.usePipeline(gl, pipeline)

    gl.uniform3fv(kernelLoc, parameterizedKernel(kernel))
    gl.uniform1f(kernelWeightLoc, kernelWeight(kernel))

    return pipeline
}

function makeKernelShader(gl: WebGLRenderingContext, kernel: number[]): gfx.ProgramInfo {
    let kernelFS = makeKernelFS(kernel)
    let program = gfx.makeShaderProgram(gl, kernelVS, kernelFS)

    return {
        program: program,
        attribLocations: {
            position: 'a_Position',
            texCoord: 'a_TexCoord',
        },
        uniformLocations: {
            texture: gl.getUniformLocation(program, 'u_Texture'),
            textureSize: gl.getUniformLocation(program, 'u_TextureSize'),
            kernel: gl.getUniformLocation(program, 'u_Kernel[0]'),
            kernelWeight: gl.getUniformLocation(program, 'u_KernelWeight'),
        },
    }
}

function parameterizedKernel(kernel: number[]): number[] {
    if (kernel.length % 2 == 0) {
        throw 'Kernel must have odd dimensions'
    }

    let side = Math.sqrt(kernel.length)
    let center = (side - 1) / 2

    let res: number[] = []
    for (let y = 0; y < side; y++) {
        for (let x = 0; x < side; x++) {
            res.push(x - center) 
            res.push(y - center)
            res.push(kernel[y * side + x])
        }
    }

    return res
}

function kernelWeight(kernel: number[]): number {
    let weight = 0
    for (let i = 0; i < kernel.length; i++) {
        weight += kernel[i]
    }
    return weight <= 0 ? 1 : weight
}

const kernelVS = `
    attribute vec4 a_Position;
    attribute vec2 a_TexCoord;
    varying vec2 v_TexCoord;
    void main() {
        gl_Position = a_Position;
        v_TexCoord = a_TexCoord;
    }
`

function makeKernelFS(kernel: number[]) {
    let length = kernel.length
    return `
        precision mediump float;
        uniform sampler2D u_Texture;
        uniform vec2 u_TextureSize;
        uniform vec3 u_Kernel[${length}];
        uniform float u_KernelWeight;
        varying vec2 v_TexCoord;
        void main() {
            vec2 onePixel = vec2(1.0, 1.0) / u_TextureSize;
            vec4 colorSum = vec4(0.0, 0.0, 0.0, 0.0);
            for (int i = 0; i < ${length}; i++) {
                vec2 coord = v_TexCoord + onePixel * u_Kernel[i].xy;
                colorSum += texture2D(u_Texture, coord) * u_Kernel[i].z;
            }
            gl_FragColor = vec4(colorSum.rgb / u_KernelWeight, 1.0);
        }
    `
}

export class Pipeline {
    private gl: WebGLRenderingContext
    private width: number
    private height: number
    private source: tex.RenderTarget
    private destination: tex.RenderTarget
    private passes: number[][] = []

    constructor(gl: WebGLRenderingContext, image: HTMLImageElement) {
        let width = image.width
        let height = image.height
        this.width = width
        this.height = height
        this.gl = gl

        this.source = tex.makeRenderTarget(gl, width, height)
        let sourceTex = tex.makeTexture(gl, width, height)
        gl.bindTexture(gl.TEXTURE_2D, sourceTex)
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image)
        tex.attachTexture(gl, this.source, sourceTex)

        this.destination = tex.makeRenderTarget(gl, width, height)
        tex.attachTexture(gl, this.destination, tex.makeTexture(gl, width, height))
    }

    pipe(kernel: number[]): Pipeline {
        this.passes.push(kernel)
        return this
    }

    private exec() {
        while (this.passes.length > 0) {
            let k = this.passes.shift() as number[]
            this.step(k)
        }
    }

    private step(k: number[]) {
        let gl = this.gl

        tex.useRenderTarget(gl, this.destination)
    
        let pipeline = makeKernelPipeline(gl, makeKernelState(gl), k)
        gfx.usePipeline(gl, pipeline)
        this.setUniforms(pipeline.programInfo)
        gfx.render(gl, pipeline)

        let tmp = this.source
        this.source = this.destination
        this.destination = tmp
    }
    
    private setUniforms(programInfo: ProgramInfo) {
        let gl = this.gl

        let textureLoc = programInfo.uniformLocations.texture
        if (! util.isDefined(textureLoc)) {
            throw 'Uniform location is undefined'
        }

        let textureSizeLoc = programInfo.uniformLocations.textureSize
        if (! util.isDefined(textureSizeLoc)) {
            throw 'Uniform location is undefined'
        }

        gl.activeTexture(gl.TEXTURE0)
        gl.bindTexture(gl.TEXTURE_2D, this.source.texture as WebGLTexture)
        gl.uniform1i(textureLoc, 0)
        gl.uniform2f(textureSizeLoc, this.width, this.height)
    }

    private execAndRead(canvas: HTMLCanvasElement) {
        let gl = this.gl
        let width = this.width
        let height = this.height

        this.exec()

        let data = new Uint8Array(width * height * 4)
        tex.useRenderTarget(gl, this.source)
        gl.readPixels(0, 0, width, height, gl.RGBA, gl.UNSIGNED_BYTE, data)

        canvas.width = width
        canvas.height = height

        let context = canvas.getContext('2d')
        if (! util.isDefined(context)) {
            throw 'Failed to get 2D rendering context'
        }

        let canvasData = context.createImageData(width, height)
        canvasData.data.set(data)
        context.putImageData(canvasData, 0, 0)
    }

    private asCanvas(): HTMLCanvasElement {
        let canvas = document.createElement('canvas')
        this.execAndRead(canvas)
        return canvas
    }

    intoImage(imageId: string) {
        let canvas = this.asCanvas()

        let image = document.getElementById(imageId)
        if (! (image instanceof HTMLImageElement)) {
            throw `No DOM image element with id: ${imageId}`
        }
        image.src = canvas.toDataURL()
    }
}